var MainScene = function(){};

var controller;
var arrowLeft;

MainScene.prototype.onDidLoadFromCCB = function()
{
	if( (sys.os == "android" || sys.os == "Android")) {
		this.rootNode.setKeypadEnabled(true);
	}
	
	controller = this.rootNode;
	this.init();
	this.rootNode.setTouchEnabled(true);
	
	this.rootNode.onTouchesBegan = function(touches, event) {	
		this.controller.onTouchesBegan(touches, event);
		return true;
    }	
        
	this.rootNode.onTouchesMoved = function(touches, event) {	
		this.controller.onTouchesMoved(touches, event);
		return true;
    }	
	
	this.rootNode.onEnterTransitionDidFinish = function() {
		setStopAction(null, false);
		return true;
	}
}

MainScene.prototype.onTouchesBegan = function(touches, event) {
	var touch = touches[0];

	if (cc.rectContainsPoint(arrowLeft.getBoundingBox(), touch.getLocation())) {
		var scene = cc.BuilderReader.load("PhysicsScene");
    	cc.Director.getInstance().replaceScene(scene);
	}
	
	return true;
}

MainScene.prototype.init = function() {
	arrowLeft = this.arrowLeft;
}



